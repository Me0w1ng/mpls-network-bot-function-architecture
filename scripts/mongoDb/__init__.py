#################################################################
######################## Sira Version 2.0.1 #####################
#################################################################

from pymongo import MongoClient
from bson.objectid import ObjectId

class __database:
    def __init__(self, dname, cname):
        self.db_name = dname
        self.collection_name = cname
    ##########################
    def _add(self, data):
        try:
            client = MongoClient(port=27017)
            db = client[self.db_name]
            result = db[self.collection_name].insert_one(data)
            ID = result.inserted_id
            client.close()
            return [1, 'INSERTING A NEW RECORD TO MONGO DATABASE SUCCESSFULLY (_add)', ID ]
        except Exception as error:
            return [0, 'FAILED INSERTING A NEW RECORD TO MONGO DATABASE (_add)', format(error)]
    ##########################
    def _addList(self, data):
        try:
            client = MongoClient(port=27017)
            db = client[self.db_name]
            result = db[self.collection_name].insert_many(data)
            IDS = result.inserted_ids
            client.close()
            return [1, 'INSERTING NEW RECORDS TO MONGO DATABASE SUCCESSFULLY (_addList)', IDS ]
        except Exception as error:
            return [0, 'FAILED INSERTING NEW RECORDS TO MONGO DATABASE (_addList)', format(error)]
    ##########################
    def _view(self, data):
        try:
            client = MongoClient(port=27017)
            db = client[self.db_name]
            tb = db[self.collection_name]
            result = tb.find(data)
            RE = result
            client.close()
            return [1, 'VIEWING RECORDS IN MONGO DATABASE SUCCESSFULLY (_addList)', RE ]
        except Exception as error:
            return [0, 'FAILED VIEWING RECORDS IN MONGO DATABASE (_view)', format(error)]
    ##########################
    def _viewSortLimit(self, data, order, number):
        try:
            client = MongoClient(port=27017)
            db = client[self.db_name]
            tb = db[self.collection_name]
            result = tb.find(data).sort(order).limit(number)
            RE = result
            client.close()
            return [1, 'VIEWING SORT RECORDS BY LIMIT IN MONGO DATABASE SUCCESSFULLY (_viewSortLimit)', RE ]
        except Exception as error:
            return [0, 'FAILED VIEWING SORT RECORDS BY LIMIT IN MONGO DATABASE (_viewSortLimit)', format(error)]
    ##########################
    def _edit(self, data, key):
        try:
            client = MongoClient(port=27017)
            db = client[self.db_name]
            tb = db[self.collection_name]
            result = tb.update_many(key, data)
            RE = result
            client.close()
            return [1, 'EDITING RECORDS IN MONGO DATABASE SUCCESSFULLY (_edit)', RE ]
        except Exception as error:
            return [0, 'FAILED EDITING RECORDS IN MONGO DATABASE (_edit)', format(error)]
    ##########################
    def _editId(self, data, oid):
        try:
            client = MongoClient(port=27017)
            db = client[self.db_name]
            tb = db[self.collection_name]
            result = tb.update_one(oid, data) 
            RE = result
            client.close()
            return [1, 'EDITING RECORD BY ID IN MONGO DATABASE SUCCESSFULLY (_editId)', RE ]
        except Exception as error:
            return [0, 'FAILED EDITING RECORD BY ID IN MONGO DATABASE (_editId)', format(error)]
    ##########################
    def _deleteBy(self, data):
        try:
            client = MongoClient(port=27017)
            db = client[self.db_name]
            tb = db[self.collection_name]
            result = tb.delete_many(data)
            RE = [ result, result.deleted_count ]
            client.close()
            return [1, 'DELETE RECORD IN MONGO DATABASE SUCCESSFULLY (_deleteBy)', RE ]
        except Exception as error:
            return [0, 'FAILED DELETE RECORD IN MONGO DATABASE (_deleteBy)', format(error)]

class mongoDb(__database):
   def add(self, data): return self._add(data) 
   def addList(self, data): return self._addList(data) 
   def view(self, data): return self._view(data) 
   def viewSortLimit(self, data, order, number): return self._viewSortLimit(data, order, number) 
   def edit(self, data, key): return self._edit(data, key) 
   def editId(self, data, oid): return self._editId(data, oid) 
   def deleteBy(self, data): return self._deleteBy(data) 



#################################################################
######################## Sira Version 2.0.1 #####################
#################################################################
#-------------------------------------------
#-------------------------------------------
#db = mongoDb("mpls", "pe")
#oid = {'_id': ObjectId('5db96364b395dd5412871b38')}
#data = { "$set": { "name": "Rachitha", "address": "colombo" }}
#cursor = db.editId(data, oid)
#cursorX = db.view({})
#for document in cursorX[1]:
#          print(document)
#-------------------------------------------
#-------------------------------------------
#db = mongoDb("mpls", "pe")
#key = { "name": "Saman" }
#data = { "$set": { "name": "kkk" }}
#cursor = db.edit(data, key)
#print(cursor[1].modified_count)
#cursorX = db.view({})
#for document in cursorX[1]:
#          print(document)
#-------------------------------------------
#-------------------------------------------
#db = mongoDb("mpls", "pe")
#mydict = { "name": "John" }
#cursor = db.viewSortLimit(mydict, "name", 5)
#for document in cursor[1]:
#    print(document)
#-------------------------------------------
#-------------------------------------------
#db = mongodb("mpls", "pe")
#mydict = { }
#cursor = db.view(mydict)
#for document in cursor[1]:
#          print(document)
#-------------------------------------------
#-------------------------------------------
#db = mongoDb("mpls", "pe")
#mydict = { "name": "John" }
#x = db.add(mydict)
#print(x)
#-------------------------------------------
#-------------------------------------------
#db = mongoDb("mpls", "pe")
#mydict = [ { "name": "John", "address": "Highway 37" }, { "name": "John", "address": "Highway 37" }]  
#x = db.addlist(mydict)
#print(x)
#-------------------------------------------
#-------------------------------------------

#################################################################

   
