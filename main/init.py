#################################################################
######################## Sira Version 2.0.1 #####################
#################################################################
import sys
import os
import datetime
import time
#from mysqlDb import mysqlDb
#from mongoDb import mongoDb
from urlRequest import urlRequest
from mailServer import mailServer
#import writeData
#import logMsg
#import serverCom

def stdOutPrint(DATA):
     sys.stdout.write(now.strftime("%Y-%m-%d %H:%M:%S")+' | '+str(DATA)+'\n')
     sys.stdout.flush()

#########################################################################
############################  MAIN FUNCTION  ############################
#########################################################################
def main(config_json):
     stdOutPrint('LOADING CONFIG JSON FILE')
     # LOADING CORE JUMP SERVERS LOGIN CONFIG
     JUMP_SERVER_48 = config_json["CONFIG"]["JUMP_SERVER_48"]
     JUMP_SERVER_11 = config_json["CONFIG"]["JUMP_SERVER_11"]
     # LOADING CX JUMP SERVERS LOGIN CONFIG
     CX_JUMP_SERVER_157 = config_json["CONFIG"]["CX_JUMP_SERVER_157"]
     CX_JUMP_SERVER_160 = config_json["CONFIG"]["CX_JUMP_SERVER_160"]
     # LOADING DOMAIN USER LOGIN CONFIG
     DOMAIN_USER_CONFIG = config_json["CONFIG"]["DOMAIN_USER"]
     # LOADING PROXY SERVER CONFIG
     DIALOG_PROXY_SERVER = config_json["CONFIG"]["DIALOG_PROXY_SERVER"]
     # LOADING MAIL SERVER CONFIG
     DIALOG_MAIL_SERVER = config_json["CONFIG"]["DIALOG_MAIL_SERVER"]
     ###########################################################################
     stdOutPrint('READING URL CONNECTION')



     DATA = {
          "URL_REQUEST":{
               "URL" : "http://10.62.96.11:8098/sira/ims/alarms/getAlarmPage",
               "DATA" : {
                   "page":0,
                   "limit":10000000,
                   "status":null,
                   "alarmType":null,
                   "source":null,
                   "createdBy":null,
                   "solution":null,
                   "reference":null,
                   "alarmTime_start":"2020-06-28 00:00:00",
                   "alarmTime_end":"2020-06-28 23:59:59",
                   "order":"DESC",
                   "sortBy":"alarm_time"
               }
          },
          "URL_TYPE": "POST",
          "URL_CATA": "DATA"
     }

#mail = urlRequest(DATA)
#RESUT = mail.URL()
#print(RESUT)
#RESUT = mail.CALL({"URL" : "http://10.62.96.8:8089/onecrm/getconnectionstatus?number=994633061", "DATA" : ""})
#print(RESUT)

     
     #db = mysqlDb()
     #msg = logMsg.EN()
     #CORE_DEVICE = db.viewAll("mpls_core_device_tb", " type = 'PE' AND active=1 ")
     #stdOutPrint('CORE DEVICES : BE AND PE AND LNS DEVICES')
     #JUMP_SESSION = []
     #DEVICE_SESSION = []

     '''
     for DEVICE in CORE_DEVICE[2][0]:
          printHeader()
          print('▀▀    '+str(DEVICE[1])+' : '+str(DEVICE[3])+' ('+str(DEVICE[2])+')')
          if(JUMP_SESSION == []):
               DEVICE_SESSION = serverCom.NET(JUMP_SERVER_48, DOMAIN_USER_CONFIG, DEVICE)
               JUMP_SESSION = DEVICE_SESSION.JUMP()
               print('▀▀    INITIATE JUMP SESSION')
          else:
               JUMP_SESSION = DEVICE_SESSION.RETURN_JUMP(DEVICE)
               print('▀▀    RETURN TO JUMP SESSION')
          ####################################################
          print('▀▀    '+msg.get(str(JUMP_SESSION[0][1])))
          if(JUMP_SESSION[0][0]):
               if(DEVICE[11]=="TELNET"):
                    TELNET_SESSION = DEVICE_SESSION.TELNET()
                    print('▀▀    '+msg.get(str(TELNET_SESSION[0][1])))
                    if(TELNET_SESSION[0][0]):
                         ENABLE_SESSION = DEVICE_SESSION.ENABLE_MODE()
                         print('▀▀    '+msg.get(str(ENABLE_SESSION[0][1])))
                         if(ENABLE_SESSION[0][0]):
                              COMMAND = { 'CISCO':'show running-config', 'HUAWEI':'display current-configuration', 'OTHER':'show running-config' }
                              WRITE_LONG_COMMAND = DEVICE_SESSION.WRITE_LONG(COMMAND)
                              print('▀▀    READING ALL CONFIGURATION AND '+msg.get(str(WRITE_LONG_COMMAND[0][1])))
                              if(WRITE_LONG_COMMAND[0][0]):
                                   CBACKUP = CREATE_BACKUP(DEVICE[1], DEVICE[2], ENABLE_SESSION[2][1], WRITE_LONG_COMMAND[0][2])
                                   print('▀▀    '+str(CBACKUP[1]))
                                   if(CBACKUP[0]):
                                        SBACKUP = SAVE_BACKUP(DEVICE[0], DEVICE[1], DEVICE[2], DEVICE[3], DEVICE[5], DEVICE[6], DEVICE[7], str(WRITE_LONG_COMMAND[0][2]))
                                        print('▀▀    '+str(SBACKUP[1]))
                                        if(SBACKUP[0]):
                                             DONE_SD = STATUS_DEVICE(str(DEVICE[1]), 0, '')
                                             print('▀▀    '+str(DONE_SD[1]))
                                        else: print('▀▀    '+str(SBACKUP[2]))
                                   else: print('▀▀    '+str(CBACKUP[2]))
                              else:
                                   print('▀▀    '+str(WRITE_LONG_COMMAND[0][2]))
                                   WLC_SD = STATUS_DEVICE(str(DEVICE[2]), 1, str(WRITE_LONG_COMMAND[0][1]))
                                   if(WLC_SD[0]): print('▀▀    '+str(WLC_SD[1]))
                                   else: print('▀▀    '+str(WLC_SD[2]))
                         else:
                              print('▀▀    '+str(ENABLE_SESSION[0][2]))
                              ENABLE_DD = DEACTIVE_DEVICE(str(DEVICE[2]), str(ENABLE_SESSION[0][1]))
                              if(ENABLE_DD[0]): print('▀▀    '+str(ENABLE_DD[1]))
                              else: print('▀▀    '+str(ENABLE_DD[2]))
                    else:
                         print('▀▀    '+str(TELNET_SESSION[0][2]))
                         TELNET_DD = DEACTIVE_DEVICE(str(DEVICE[2]), str(TELNET_SESSION[0][1]))
                         if(TELNET_DD[0]): print('▀▀    '+str(TELNET_DD[1]))
                         else: print('▀▀    '+str(TELNET_DD[2]))
               else: print('▀▀    CAN NOT FIND THE PROTOCALL "'+str(DEVICE[11])+'" ')
          else:
               print('▀▀    '+str(JUMP_SESSION[0][2]))
               JUMP_SD = STATUS_DEVICE(str(DEVICE[2]), 1, str(JUMP_SESSION[0][1]))
               if(JUMP_SD[0]): print('▀▀    '+str(JUMP_SD[1]))
               else: print('▀▀    '+str(JUMP_SD[2]))
     MAIL = SEND_MAIL(DIALOG_MAIL_SERVER, '<h1>HELLOW BODY COMPLETE</h1><br>This is test mail')
     print(MAIL)
     printFooter()
     '''




#########################################################################
def SEND_MAIL(SERVER, BODY):
     try:
          REPORTING_DATE = datetime.datetime.now().date()
          DATA = {
               "Subject": "DAILY BACKUP BE / PE / LNS : "+str(REPORTING_DATE),
               "To": [ "Rachitha <rachitha.jeewandara@dialog.lk>" ],
               "Cc": [],
               "Body": str(BODY),
               "Attachment": []
          }
          SESSION = mailServer(SERVER)
          RESUT = SESSION.SEND(DATA)
          return RESUT
     except Exception as error: return [ 0, 'ERROR OCCURRED IN SEND_MAIL FUNCTION', str(error) ]

#########################################################################
def DEACTIVE_DEVICE(DEVICE_IP, COMMENT):
    try:
        db = mysqlDb()
        RESULT = db.edit("mpls_core_device_tb", " comment='"+str(COMMENT)+"', active=0 ", " ip='"+str(DEVICE_IP)+"' ")
        return RESULT
    except Exception as error: return [ 0, 'ERROR OCCURRED IN DEACTIVE_DEVICE FUNCTION', str(error) ]

#########################################################################
def STATUS_DEVICE(DEVICE_IP, STATUS, COMMENT):
    try:
        db = mysqlDb()
        RESULT = db.edit("mpls_core_device_tb", " comment='"+str(COMMENT)+"', status=1 ", " ip='"+str(DEVICE_IP)+"' ")
        return RESULT
    except Exception as error: return [ 0, 'ERROR OCCURRED IN STATUS_DEVICE FUNCTION', str(error) ]

#########################################################################
def CREATE_BACKUP(DEVICE_TYPE, DEVICE_IP, DEVICE_CATA, CONFIG_TEXT):
    try:
        BACKUP = writeData.config(str(DEVICE_TYPE), str(DEVICE_IP)+"__"+str(DEVICE_CATA), str(CONFIG_TEXT))
        return BACKUP
    except Exception as error: return [0, 'ERROR OCCURRED IN CREATE_BACKUP FUNCTION', str(error) ]

#########################################################################
def SAVE_BACKUP(DEVICE_ID, DEVICE_TYPE, DEVICE_IP, DEVICE_DOMAIN, DEVICE_BRAND, DEVICE_CATEGORY, DEVICE_POSITION, CONFIG_TEXT):
    try:
        REPORTING_DATE = datetime.datetime.now().date()
        REPORTING_TIME = datetime.datetime.now().time()
        db = mongoDb('mpls_db', "core_"+str(DEVICE_TYPE)+"_config_log")
        SQL_DATA_STRING = {
            'DID': str(DEVICE_ID), 'TYPE': str(DEVICE_TYPE), 'IP': str(DEVICE_IP), 'DOMAIN': str(DEVICE_DOMAIN),
            'BRAND': str(DEVICE_BRAND), 'CATEGORY': str(DEVICE_CATEGORY), 'POSITION': str(DEVICE_POSITION),
            'CONFIG': str(CONFIG_TEXT), 'REPORTING_DATE': str(REPORTING_DATE), 'REPORTING_TIME': str(REPORTING_TIME)
        }
        BACKUP = db.add(SQL_DATA_STRING)
        return BACKUP
    except Exception as error: return [0, 'ERROR OCCURRED IN SAVE_BACKUP FUNCTION', str(error) ]



