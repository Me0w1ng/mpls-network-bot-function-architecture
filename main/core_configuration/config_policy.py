#################################################################
######################## Sira Version 2.0.1 #####################
#################################################################

import re
from mysqldb import mysqldb
from mongodb import mongodb
import logmsg
import datetime

class saveBePolicyMap:
    def __init__(self, config_data, device_data):
        config_list = []                                                # SAVE CONFIGURATION DATA TO A ARRAY
        REPORT_DATE = str(datetime.datetime.now().date())               # GET REPORING DATE
        for i in range(len(config_data)):
            CONFIG_SET = 'policy-map '+str(config_data[i])+"\n!"
            config_list.append({ "did" : str(device_data['id']), "dip" : str(device_data['ip']), "type": "policy_map", "configuration" : str(CONFIG_SET), "date" : str(REPORT_DATE) })
        mondb = mongodb("mpls", "core_be_configuration_log")
        x = mondb.addList(config_list)

class savePePolicyMap:
    def __init__(self, config_data, device_data):
        config_list = []                                                # SAVE CONFIGURATION DATA TO A ARRAY
        REPORT_DATE = str(datetime.datetime.now().date())               # GET REPORING DATE
        for i in range(len(config_data)):
            CONFIG_SET = 'policy-map '+str(config_data[i])+"\n"
            config_list.append({ "did" : str(device_data['id']), "dip" : str(device_data['ip']), "type": "policy_map", "configuration" : str(CONFIG_SET), "date" : str(REPORT_DATE) })
        mondb = mongodb("mpls", "core_pe_configuration_log")
        x = mondb.addList(config_list)

class saveBeRoutePolicy:
    def __init__(self, config_data, device_data):
        config_list = []                                                # SAVE CONFIGURATION DATA TO A ARRAY
        REPORT_DATE = str(datetime.datetime.now().date())               # GET REPORING DATE
        for i in range(len(config_data)):
            CONFIG_SET = 'route-policy '+str(config_data[i])+"\n!"
            config_list.append({ "did" : str(device_data['id']), "dip" : str(device_data['ip']), "type": "route_policy", "configuration" : str(CONFIG_SET), "date" : str(REPORT_DATE) })
        mondb = mongodb("mpls", "core_be_configuration_log")
        x = mondb.addList(config_list)

class savePeRoutePolicy:
    def __init__(self, config_data, device_data):
        config_list = []                                                # SAVE CONFIGURATION DATA TO A ARRAY
        REPORT_DATE = str(datetime.datetime.now().date())               # GET REPORING DATE
        for i in range(len(config_data)):
            CONFIG_SET = 'route-policy '+str(config_data[i])+"\n!"
            config_list.append({ "did" : str(device_data['id']), "dip" : str(device_data['ip']), "type": "route_policy", "configuration" : str(CONFIG_SET), "date" : str(REPORT_DATE) })
        mondb = mongodb("mpls", "core_pe_configuration_log")
        x = mondb.addList(config_list)

