#################################################################
######################## Sira Version 2.0.1 #####################
#################################################################

from netmiko import ConnectHandler
import re
from mysqldb import mysqldb
import write
import datetime
import validate
import core_configuration
import logmsg


class __DEVICE:
    def __init__(self, jump_server_login_user, device_login_user, device_data):
        try:
            # CONFIGERATION DECLARATION
            self.CONFIG_TEXT = ""
            self.JUMP_SERVER_SESSION = jump_server_login_user
            self.JSERVER = []
            self.LOGIN_USER = device_login_user['username']
            self.LOGIN_PASS = device_login_user['password']
            # DEVICE DATA DECLARATION
            self.DEVICE_ID = device_data[0]
            self.DEVICE_TYPE = device_data[1]
            self.DEVICE_IP = device_data[2]
            self.DEVICE_NAME = device_data[3]
            self.DEVICE_BRAND = device_data[4]
            self.DEVICE_HOST_NAME = device_data[5]
            self.DEVICE_SYMBOLE = device_data[6]
            self.DEVICE_PASS = device_data[7]
            self.DEVICE_ACTIVE = device_data[8]
            self.LOG = logmsg.log_and_msg(device_data[2])
            #########################################################################
            # ADDITIONAL DECLARATION
            #########################################################################
            #########################################################################
            #########################################################################
            self.CONFIG_VRF = 0
            self.CONFIG_VLAN = 0
            self.CONFIG_IPV4 = 0
            self.CONFIG_IP_ROUTE_VRF = 0
            self.CONFIG_IP_ACCESS_LIST = 0
            self.CONFIG_IP_PREFIX_LIST = 0 
            self.CONFIG_CLASS_MAP = 0
            self.CONFIG_POLICY_MAP = 0
            self.CONFIG_INTERFACE = 0
            self.CONFIG_SERVICE_INSTANCE = 0
            self.CONFIG_L2VPN = 0
            self.CONFIG_ROUTE_POLICY = 0
            self.CONFIG_ROUTE_STATIC = 0
            self.CONFIG_ROUTE_ISIS = 0
            self.CONFIG_ROUTE_BGP = 0
            self.CONFIG_ROUTE_BGP_ADDRESS_FAMILY = 0
            self.CONFIG_ROUTE_VRRP = 0
            self.CONFIG_ROUTE_PIM = 0
            self.CONFIG_MULTICAST_ROUTING = 0
            self.LOG.LM('INITIATION_D')
        except:
            self.LOG.LM('INITIATION_E')

    #########################################################################
    def _JUMP(self):
        try:
            self.JSERVER = ConnectHandler(**self.JUMP_SERVER_SESSION)
            OUT = self.JSERVER.send_command_timing(' ', strip_command=False, strip_prompt=False)
            JUMP_LOGIN = validate.SPLIST("@", OUT)
            if(JUMP_LOGIN[1] == 'ispmon ~]$ '):
                self.LOG.LM('LOGIN_JUMP_D', 'USER : '+str(self.JUMP_SERVER_SESSION['username']))
                return 1
            else:
                self.LOG.LM('LOGIN_JUMP_F')
                return 0
        except:
            self.LOG.LM('LOGIN_JUMP_E')
            return 0

    #########################################################################
    def _TELNET(self):
        try:
            OUT = self.JSERVER.send_command_expect( 'telnet ' + str(self.DEVICE_IP), 'Username')
            OUT += self.JSERVER.send_command_expect( str(self.LOGIN_USER), 'Password')
            OUT += self.JSERVER.send_command_timing( str(self.LOGIN_PASS) + '\n', normalize=False)
            self.LOG.LM('TELNET_D')
            return 1
        except:
            self.LOG.LM('TELNET_E')
            return 0

    #########################################################################
    def _FIND_ROUTER_TYPE(self):
        try:
            DATA = self.JSERVER.send_command_timing( '\n\n', normalize=False)
            ROUTER_HOST_NAME = re.findall('<(.*?)>', DATA, re.DOTALL)
            if(ROUTER_HOST_NAME!=[]): EN = [1, "HUAWEI", 0, ROUTER_HOST_NAME[0]]
            else:
                ROUTER_HOST_NAME = re.findall('[(.*?)]', DATA, re.DOTALL)
                if(ROUTER_HOST_NAME!=[]): EN = [1, "HUAWEI", 1, ROUTER_HOST_NAME[0]]
                else:
                    ROUTER_HOST_NAME = re.findall('(.*?)>', DATA, re.DOTALL)
                    if(ROUTER_HOST_NAME!=[]): EN = [1, "CISCO", 0, ROUTER_HOST_NAME[0]]
                    else:
                        ROUTER_HOST_NAME = re.findall('(.*?)#', DATA, re.DOTALL)
                        if(ROUTER_HOST_NAME!=[]): EN = [1, "CISCO", 1, ROUTER_HOST_NAME[0]]
                        else: EN = [0, "UNKNOWN", 0, self.DEVICE_IP]
            if(EN[3]=='.'): EN = [0, "NO_ACCESS", 0, self.DEVICE_IP]
            self.LOG.LM('FIND_ROUTER_TYPE_D')
            return EN
        except:
            self.LOG.LM('FIND_ROUTER_TYPE_E')
            return [0, "EXCEPTION", 0, self.DEVICE_IP]

    #########################################################################
    def _ENABLE_MODE(self):
        try:
            EN = self._FIND_ROUTER_TYPE()
            if(EN[2]):
                self.LOG.LM('ENABLE_MODE_D')
                return 1
            else:
                if(self.DEVICE_BRAND=="HUAWEI"):
                    OUT = self.JSERVER.send_command_timing("sys\n", normalize=False)
                    OUT += self.JSERVER.send_command_timing( str(self.DEVICE_PASS) + '\n', normalize=False)
                    EN = self._FIND_ROUTER_TYPE()
                    print("Step 22")
                    if(EN[2]):
                        self.LOG.LM('ENABLE_MODE_D')
                        return 1
                    else:
                        self.LOG.LM('ENABLE_MODE_F', 'ROUTER : '+str(EN[1])+'|'+str(EN[3]))
                        return 0
                elif(self.DEVICE_BRAND=="CISCO"):
                    OUT = self.JSERVER.send_command_timing("en\n", normalize=False)
                    OUT += self.JSERVER.send_command_timing( str(self.DEVICE_PASS) + '\n', normalize=False)
                    EN = self._FIND_ROUTER_TYPE()
                    if(EN[2]):
                        self.LOG.LM('ENABLE_MODE_D')
                        return 1
                    else:
                        self.LOG.LM('ENABLE_MODE_F', 'ROUTER : '+str(EN[1])+'|'+str(EN[3]))
                        return 0
                else:
                    self.LOG.LM('ENABLE_MODE_F', 'ROUTER : '+str(EN[1])+'|'+str(EN[3]))
                    return 0
        except:
            self.LOG.LM('ENABLE_MODE_E')
            return 0

    #########################################################################
    def _DEACTIVE_DEVICE(self, COMMENT):
        try:
            db = mysqldb()
            RESULT = db.edit("mpls_core_device_tb", " comment='"+str(COMMENT)+"' AND status=0 ", " ip='"+str(self.DEVICE_IP)+"' ")
            self.LOG.LM('DEACTIVE_DEVICE_D')
            return 1
        except:
            self.LOG.LM('DEACTIVE_DEVICE_E')
            return 0

    #########################################################################
    def _CREATE_BACKUP(self):
        try:
            BACKUP = write.config(str(self.DEVICE_TYPE), str(self.DEVICE_IP), str(self.CONFIG_TEXT))
            if(BACKUP[0]):
                self.LOG.LM('CREATE_BACKUP_C')
                return 1
            else:
                self.LOG.LM('CREATE_BACKUP_F')
                return 0
        except:
            self.LOG.LM('CREATE_BACKUP_E')
            return 0















    #########################################################################
    #########################################################################
    #########################################################################
    #########################################################################
    #########################################################################
    #########################################################################
    ##########################  DEFAULT FUNCTIONS  ##########################
    #########################################################################
    #########################################################################
    #########################################################################
    #########################################################################
    #########################################################################
    #########################################################################


    #########################################################################
    def _SHOW_RUN(self):
        try:
            # READING CONFIGURATION DO WHILE LOOP
            # --- DO ---
            self.LOG.LM('SHOW_RUN_S')
            OUT = self.JSERVER.send_command_timing("sh run\n", normalize=False)
            OUT += self.JSERVER.send_command_timing("                                                                                                   ", normalize=False)
            self._READ_CONFIGURATION(OUT)
            # --- WHILE ---
            while True:
                if "--More--" in OUT:
                    OUT = self.JSERVER.send_command_timing("                                                                                                                        ", normalize=False)
                    self._READ_CONFIGURATION(OUT)
                else:
                    self.LOG.LM('READ_CONFIGURATION_C')
                    break
            self.LOG.LM('SHOW_RUN_C')
            return 1
        except:
            self.LOG.LM('SHOW_RUN_E')
            return 0

    #########################################################################
    def _READ_CONFIGURATION(self, DATA):
        try:
            DATA = re.sub(r' --More--         ', '', DATA)
            DATA = re.sub(r' --More--  ', '', DATA)
            self.CONFIG_TEXT = self.CONFIG_TEXT + DATA
            self.LOG.LM('READ_CONFIGURATION_S')
            return 1
        except:
            self.LOG.LM('READ_CONFIGURATION_E')
            return 0


    #########################################################################
    def _CAPTURE_CONFIGURATION(self):
        try:
            self.LOG.LM('READ_CONFIGURATION_S')
            # GET REPORING DATE
            REPORT_DATE = str(datetime.datetime.now().date())
            # COLLECT DEVICE DATA TO JSON
            DEV_DATA = {
                "id" : str(self.DEVICE_ID),
                "ip" : str(self.DEVICE_IP),
                "type" : str(self.DEVICE_TYPE),
                "host" : str(self.DEVICE_NAME),
                "brand" : str(self.DEVICE_BRAND)
            }
            """
            ##---------------------------------------------------------##
            # VRF
            VRF_DATA = re.findall('\nip vrf (.*?)\n!', self.CONFIG_TEXT, re.DOTALL)
            if(VRF_DATA != []):
                core_configuration.savePeVrf(VRF_DATA, DEV_DATA)
                self.CONFIG_VRF = self.CONFIG_VRF + len(VRF_DATA)
            ##---------------------------------------------------------##
            # VLAN
            VLAN_DATA = re.findall('\nvlan (.*?)\n!', self.CONFIG_TEXT, re.DOTALL)
            if(VLAN_DATA != []):
                core_configuration.savePeVlan(VLAN_DATA, DEV_DATA)
                self.CONFIG_VLAN = self.CONFIG_VLAN + len(VLAN_DATA)
            ##---------------------------------------------------------##
            # CLASS-MAP
            CLASS_MAP_DATA = re.findall('lass-map (.*?)\n\S', self.CONFIG_TEXT, re.DOTALL)
            if(CLASS_MAP_DATA != []):
                core_configuration.savePeClassMap(CLASS_MAP_DATA, DEV_DATA)
                self.CONFIG_CLASS_MAP = self.CONFIG_CLASS_MAP + len(CLASS_MAP_DATA)
            ##---------------------------------------------------------##
            # POLICY-MAP
            POLICY_MAP_DATA = re.findall('olicy-map (.*?)\n\S', self.CONFIG_TEXT, re.DOTALL)
            if(POLICY_MAP_DATA != []):
                core_configuration.savePePolicyMap(POLICY_MAP_DATA, DEV_DATA)
                self.CONFIG_POLICY_MAP = self.CONFIG_POLICY_MAP + len(POLICY_MAP_DATA)
            ##---------------------------------------------------------##
            # INTERFACE
            INTERFACE_DATA = re.findall('\ninterface (.*?)\n!', self.CONFIG_TEXT, re.DOTALL)
            if(INTERFACE_DATA != []):
                core_configuration.savePeInterface(INTERFACE_DATA, DEV_DATA)
                self.CONFIG_INTERFACE = self.CONFIG_INTERFACE + len(INTERFACE_DATA)
            # SERVICE INSTANCE
            SERVICE_INSTANCE_DATA = re.findall('\n service instance (.*?)\n !', self.CONFIG_TEXT, re.DOTALL)
            if(SERVICE_INSTANCE_DATA != []):
                core_configuration.savePeServiceInstance(SERVICE_INSTANCE_DATA, DEV_DATA)
                self.CONFIG_SERVICE_INSTANCE = self.CONFIG_SERVICE_INSTANCE + len(SERVICE_INSTANCE_DATA)
            ##---------------------------------------------------------## 
            # ROUTER ISIS
            ROUTER_ISIS_DATA = re.findall('\nrouter isis (.*?)\n!', self.CONFIG_TEXT, re.DOTALL)
            if(ROUTER_ISIS_DATA != []):
                core_configuration.savePeRouterIsis(ROUTER_ISIS_DATA, DEV_DATA)
                self.CONFIG_ROUTE_ISIS = self.CONFIG_ROUTE_ISIS + len(ROUTER_ISIS_DATA)
            ##---------------------------------------------------------##
            # ROUTER BGP
            ROUTER_BGP_DATA = re.findall('\nrouter bgp (.*?)\n!', self.CONFIG_TEXT, re.DOTALL)
            if(ROUTER_BGP_DATA != []):
                core_configuration.savePeRouterBgp(ROUTER_BGP_DATA, DEV_DATA)
                self.CONFIG_ROUTE_BGP = self.CONFIG_ROUTE_BGP + len(ROUTER_BGP_DATA)
            # ROUTER BGP ADDRESS FAMILY
            ROUTER_BGP_ADDRESS_FAMILY_DATA = re.findall('\n address-family (.*?)\n exit-address-family\n !', self.CONFIG_TEXT, re.DOTALL)
            if(ROUTER_BGP_ADDRESS_FAMILY_DATA != []):
                core_configuration.savePeRouterBgpAddressFamily(ROUTER_BGP_ADDRESS_FAMILY_DATA, DEV_DATA)
                self.CONFIG_ROUTE_BGP_ADDRESS_FAMILY = self.CONFIG_ROUTE_BGP_ADDRESS_FAMILY + len(ROUTER_BGP_ADDRESS_FAMILY_DATA)
            ##---------------------------------------------------------## 
            # IP ROUTE VRF
            IP_ROUTE_VRF_DATA = re.findall('ip route vrf (.*?)\n', self.CONFIG_TEXT, re.DOTALL)
            if(IP_ROUTE_VRF_DATA != []):
                core_configuration.savePeIpRouteVrf(IP_ROUTE_VRF_DATA, DEV_DATA)
                self.CONFIG_IP_ROUTE_VRF = self.CONFIG_IP_ROUTE_VRF + len(IP_ROUTE_VRF_DATA)
            # IP ACCESS LIST
            IP_ACCESS_LIST_DATA = re.findall('p access-list (.*?)\n\S', self.CONFIG_TEXT, re.DOTALL)
            if(IP_ACCESS_LIST_DATA != []):
                core_configuration.savePeIpAccessList(IP_ACCESS_LIST_DATA, DEV_DATA)
                self.CONFIG_IP_ACCESS_LIST = self.CONFIG_IP_ACCESS_LIST + len(IP_ACCESS_LIST_DATA)
            # IP PREFIX LIST
            IP_PREFIX_LIST_DATA = re.findall('ip prefix-list (.*?)\n', self.CONFIG_TEXT, re.DOTALL)
            if(IP_PREFIX_LIST_DATA != []):
                core_configuration.savePeIpPrefixList(IP_PREFIX_LIST_DATA, DEV_DATA)
                self.CONFIG_IP_PREFIX_LIST = self.CONFIG_IP_PREFIX_LIST + len(IP_PREFIX_LIST_DATA)
            ##---------------------------------------------------------##
            # ROUTE MAP
            ROUTE_MAP_DATA = re.findall('route-map (.*?)\n!', self.CONFIG_TEXT, re.DOTALL)
            if(ROUTE_MAP_DATA != []):
                core_configuration.savePeRouteMap(ROUTE_MAP_DATA, DEV_DATA)
                self.CONFIG_L2VPN = self.CONFIG_L2VPN + len(ROUTE_MAP_DATA)
            ##---------------------------------------------------------##
            # READING route-policy DATA
            ROUTE_POLICY_DATA = re.findall('\nroute-policy (.*?)\n!', self.CONFIG_TEXT, re.DOTALL)
            if(ROUTE_POLICY_DATA != []):
                core_configuration.savePeRoutePolicy(ROUTE_POLICY_DATA, DEV_DATA)
                self.CONFIG_ROUTE_POLICY = self.CONFIG_ROUTE_POLICY + len(ROUTE_POLICY_DATA)
                # self.LOG.LM('ROUTE_POLICY_D') else: self.LOG.LM('ROUTE_POLICY_N')
            ##---------------------------------------------------------##
            # READING router static DATA
            ROUTER_STATIC_DATA = re.findall('\nrouter static (.*?)\n!', self.CONFIG_TEXT, re.DOTALL)
            if(ROUTER_STATIC_DATA != []):
                core_configuration.savePeRouterStatic(ROUTER_STATIC_DATA, DEV_DATA)
                self.CONFIG_ROUTE_STATIC = self.CONFIG_ROUTE_STATIC + len(ROUTER_STATIC_DATA)
                # self.LOG.LM('ROUTER_STATIC_D') else: self.LOG.LM('ROUTER_STATIC_N')
            ##---------------------------------------------------------##
            # READING router vrrp DATA
            ROUTER_VRRP_DATA = re.findall('\nrouter vrrp (.*?)\n!', self.CONFIG_TEXT, re.DOTALL)
            if(ROUTER_VRRP_DATA != []):
                core_configuration.savePeRouterVrrp(ROUTER_VRRP_DATA, DEV_DATA)
                self.CONFIG_ROUTE_VRRP = self.CONFIG_ROUTE_VRRP + len(ROUTER_VRRP_DATA)
                # self.LOG.LM('ROUTER_VRRP_D') else: self.LOG.LM('ROUTER_VRRP_N')
            ##---------------------------------------------------------##
            # READING router pim DATA
            ROUTER_PIM_DATA = re.findall('\nrouter pim (.*?)\n!', self.CONFIG_TEXT, re.DOTALL)
            if(ROUTER_PIM_DATA != []):
                core_configuration.savePeRouterPim(ROUTER_PIM_DATA, DEV_DATA)
                self.CONFIG_ROUTE_PIM = self.CONFIG_ROUTE_PIM + len(ROUTER_PIM_DATA)
                # self.LOG.LM('ROUTER_PIM_D') else: self.LOG.LM('ROUTER_PIM_N')
            ##---------------------------------------------------------##
            # READING multicast-routing DATA
            MULTCAST_ROUTING_DATA = re.findall('\nmulticast-routing (.*?)\n!', self.CONFIG_TEXT, re.DOTALL)
            if(MULTCAST_ROUTING_DATA != []):
                core_configuration.savePeMulticastRouting(MULTCAST_ROUTING_DATA, DEV_DATA)
                self.CONFIG_MULTICAST_ROUTING = self.CONFIG_MULTICAST_ROUTING + len(MULTCAST_ROUTING_DATA)
                # self.LOG.LM('MULTCAST_ROUTING_D') else: self.LOG.LM('MULTCAST_ROUTING_N')
            ##---------------------------------------------------------##
            """
            self.LOG.LM('CAPTURE_CONFIGURATION_C')
            return 1
        except:
            self.LOG.LM('CAPTURE_CONFIGURATION_E')
            return 0

    #########################################################################
    def _DB_DELETE_CORE_CONFIG(self):
        try:
            db = mysqldb()
            RE = db.deleteBy("mpls_core_config_tb", " did="+str(self.DEVICE_ID)+" ")
            self.LOG.LM('DB_DELETE_CONFIG_C')
            return 1
        except:
            self.LOG.LM('DB_DELETE_CONFIG_E')
            return 0

    #########################################################################
    def _DB_INSERT_CORE_CONFIG(self):
        try:
            db = mysqldb()
            #SQL_STRING = " ("+str(self.DEVICE_ID)+", 'IP_PREFIX_LIST', "+str(self.CONFIG_IP_PREFIX_LIST)+"), ("+str(self.DEVICE_ID)+", 'IP_ACCESS_LIST', "+str(self.CONFIG_IP_ACCESS_LIST)+"), ("+str(self.DEVICE_ID)+", 'IP_ROUTE_VRF', "+str(self.CONFIG_IP_ROUTE_VRF)+"), ("+str(self.DEVICE_ID)+", 'ROUTE_BGP_ADDRESS_FAMILY', "+str(self.CONFIG_ROUTE_BGP_ADDRESS_FAMILY)+"), ("+str(self.DEVICE_ID)+", 'SERVICE_INSTANCE', "+str(self.CONFIG_SERVICE_INSTANCE)+"), ("+str(self.DEVICE_ID)+", 'VRF', "+str(self.CONFIG_VRF)+"), ("+str(self.DEVICE_ID)+", 'VLAN', "+str(self.CONFIG_VLAN)+"), ("+str(self.DEVICE_ID)+", 'IPV4', "+str(self.CONFIG_IPV4)+"), ("+str(self.DEVICE_ID)+", 'CLASS_MAP', "+str(self.CONFIG_CLASS_MAP)+"), ("+str(self.DEVICE_ID)+", 'POLICY_MAP', "+str(self.CONFIG_POLICY_MAP)+"), ("+str(self.DEVICE_ID)+", 'INTERFACE', "+str(self.CONFIG_INTERFACE)+"), ("+str(self.DEVICE_ID)+", 'L2VPN', "+str(self.CONFIG_L2VPN)+"), ("+str(self.DEVICE_ID)+", 'ROUTE_POLICY', "+str(self.CONFIG_ROUTE_POLICY)+"), ("+str(self.DEVICE_ID)+", 'ROUTE_STATIC', "+str(self.CONFIG_ROUTE_STATIC)+"), ("+str(self.DEVICE_ID)+", 'ROUTE_ISIS', "+str(self.CONFIG_ROUTE_ISIS)+"), ("+str(self.DEVICE_ID)+", 'ROUTE_BGP', "+str(self.CONFIG_ROUTE_BGP)+"), ("+str(self.DEVICE_ID)+", 'ROUTE_VRRP', "+str(self.CONFIG_ROUTE_VRRP)+"), ("+str(self.DEVICE_ID)+", 'ROUTE_PIM', "+str(self.CONFIG_ROUTE_PIM)+"), ("+str(self.DEVICE_ID)+", 'MULTICAST_ROUTING', "+str(self.CONFIG_MULTICAST_ROUTING)+") "
            SQL_STRING = """
                        ("""+str(self.DEVICE_ID)+""", 'IP_PREFIX_LIST', """+str(self.CONFIG_IP_PREFIX_LIST)+"""), 
                        ("""+str(self.DEVICE_ID)+""", 'IP_ACCESS_LIST', """+str(self.CONFIG_IP_ACCESS_LIST)+"""), 
                        ("""+str(self.DEVICE_ID)+""", 'IP_ROUTE_VRF', """+str(self.CONFIG_IP_ROUTE_VRF)+"""), 
                        ("""+str(self.DEVICE_ID)+""", 'ROUTE_BGP_ADDRESS_FAMILY', """+str(self.CONFIG_ROUTE_BGP_ADDRESS_FAMILY)+"""), 
                        ("""+str(self.DEVICE_ID)+""", 'SERVICE_INSTANCE', """+str(self.CONFIG_SERVICE_INSTANCE)+"""), 
                        ("""+str(self.DEVICE_ID)+""", 'VRF', """+str(self.CONFIG_VRF)+"""), 
                        ("""+str(self.DEVICE_ID)+""", 'VLAN', """+str(self.CONFIG_VLAN)+"""), 
                        ("""+str(self.DEVICE_ID)+""", 'IPV4', """+str(self.CONFIG_IPV4)+"""), 
                        ("""+str(self.DEVICE_ID)+""", 'CLASS_MAP', """+str(self.CONFIG_CLASS_MAP)+"""), 
                        ("""+str(self.DEVICE_ID)+""", 'POLICY_MAP', """+str(self.CONFIG_POLICY_MAP)+"""), 
                        ("""+str(self.DEVICE_ID)+""", 'INTERFACE', """+str(self.CONFIG_INTERFACE)+"""), 
                        ("""+str(self.DEVICE_ID)+""", 'L2VPN', """+str(self.CONFIG_L2VPN)+"""), 
                        ("""+str(self.DEVICE_ID)+""", 'ROUTE_POLICY', """+str(self.CONFIG_ROUTE_POLICY)+"""), 
                        ("""+str(self.DEVICE_ID)+""", 'ROUTE_STATIC', """+str(self.CONFIG_ROUTE_STATIC)+"""), 
                        ("""+str(self.DEVICE_ID)+""", 'ROUTE_ISIS', """+str(self.CONFIG_ROUTE_ISIS)+"""), 
                        ("""+str(self.DEVICE_ID)+""", 'ROUTE_BGP', """+str(self.CONFIG_ROUTE_BGP)+"""), 
                        ("""+str(self.DEVICE_ID)+""", 'ROUTE_VRRP', """+str(self.CONFIG_ROUTE_VRRP)+"""), 
                        ("""+str(self.DEVICE_ID)+""", 'ROUTE_PIM', """+str(self.CONFIG_ROUTE_PIM)+"""), 
                        ("""+str(self.DEVICE_ID)+""", 'MULTICAST_ROUTING', """+str(self.CONFIG_MULTICAST_ROUTING)+""")
                        """
            RE = db.addList("mpls_core_config_tb(did, type, number)", SQL_STRING)
            self.LOG.LM('DB_INSERT_CONFIG_C')
            return [ 1, RE]
        except:
            self.LOG.LM('DB_INSERT_CONFIG_E')
            return [0, '']



#########################################################################
#########################################################################
#########################################################################
#########################################################################
#########################################################################
#########################################################################
############################  MAIN FUNCTION  ############################
#########################################################################
#########################################################################
#########################################################################
#########################################################################
#########################################################################
#########################################################################
class CSR(__DEVICE):
    def main(self):
        if(self._JUMP()):
            if(self._TELNET()):
                if(self._ENABLE_MODE()):
                    if(self._SHOW_RUN()):
                        if(self._CREATE_BACKUP()):
                            if(self._CAPTURE_CONFIGURATION()):
                                if(self._DB_DELETE_CORE_CONFIG()):
                                    RE = self._DB_INSERT_CORE_CONFIG()
                                    if(RE[0]):
                                        print("▀▀    "+str(RE[1])+" | DONE ")
                                        print("▀▀                                                                      ▀▀\n▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀")
                                    else:
                                        self.LOG.LM('DB_INSERT_CONFIG_N')
                                        return 0
                                else:
                                    self.LOG.LM('DB_DELETE_CONFIG_N')
                                    return 0
                            else:
                                self.LOG.LM('CAPTURE_CONFIGURATION_N')
                                return 0
                        else:
                            self.LOG.LM('CREATE_BACKUP_N')
                            return 0
                    else:
                        self.LOG.LM('SHOW_RUN_N')
                        return 0
                else:
                    self._DEACTIVE_DEVICE("CAN NOT ACCESS TO ENABLE MODE")
                    self.LOG.LM('ENABLE_MODE_N')
                    return 0
            else:
                self._DEACTIVE_DEVICE("CAN NOT TELNET TO IP ADDRESS")
                self.LOG.LM('TELNET_N')
                return 0
        else:
            self._DEACTIVE_DEVICE("CAN NOT LOGIN TO JUMP SERVER")
            self.LOG.LM('LOGIN_JUMP_N')
            return 0


