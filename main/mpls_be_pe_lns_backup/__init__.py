#################################################################
######################## Sira Version 2.0.1 #####################
#################################################################
import sys
import os
from mysqlDb import mysqlDb
from mongoDb import mongoDb
from urlRequest import urlRequest
import writeData
import logMsg
import serverCom

def print_data():
     os.system('cls')
     os.system('COLOR A')
     print('▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀')
     print('▀▀                                                                      ▀▀')
     print('▀▀                ███╗   ███╗ ███╗   ██╗ ██████╗   ██████╗              ▀▀')
     print('▀▀                ████╗ ████║ ████╗  ██║ ██╔══██╗ ██╔════╝              ▀▀')
     print('▀▀                ██╔████╔██║ ██╔██╗ ██║ ██████╔╝ ██║                   ▀▀')
     print('▀▀                ██║╚██╔╝██║ ██║╚██╗██║ ██╔══██╗ ██║                   ▀▀')
     print('▀▀                ██║ ╚═╝ ██║ ██║ ╚████║ ██████╔╝ ╚██████╗              ▀▀')
     print('▀▀                ╚═╝     ╚═╝ ╚═╝  ╚═══╝ ╚═════╝   ╚═════╝              ▀▀')
     print('▀▀                          -- SIRA VER: 2.0 --                         ▀▀')   
     print('▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀')
     print('▀▀                                                                      ▀▀')
     print('▀▀                      MPLS NETWORK BACKUP CONSOLE                     ▀▀')
     print('▀▀                 PROPERTY OF DIALOG AXIATA PLC - DE TAC               ▀▀')
     print('▀▀                                                                      ▀▀')
     print('▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀')
     print('▀▀    ')

#########################################################################
############################  MAIN FUNCTION  ############################
#########################################################################
def main(config_json):
    # LOADING CORE JUMP SERVERS LOGIN CONFIG
    JUMP_SERVER_48 = config_json["CONFIG"]["JUMP_SERVER_48"]
    JUMP_SERVER_11 = config_json["CONFIG"]["JUMP_SERVER_11"]
    # LOADING CX JUMP SERVERS LOGIN CONFIG
    CX_JUMP_SERVER_157 = config_json["CONFIG"]["CX_JUMP_SERVER_157"]
    CX_JUMP_SERVER_160 = config_json["CONFIG"]["CX_JUMP_SERVER_160"]
    # LOADING DOMAIN USER LOGIN CONFIG
    DOMAIN_USER_CONFIG = config_json["CONFIG"]["DOMAIN_USER"]
    # LOADING PROXY SERVER CONFIG
    DIALOG_PROXY_SERVER = config_json["CONFIG"]["DIALOG_PROXY_SERVER"]
    # LOADING MAIL SERVER CONFIG
    DIALOG_MAIL_SERVER = config_json["CONFIG"]["DIALOG_MAIL_SERVER"]
    ###########################################################################
    db = mysqlDb()
    print_data()
    CORE_DEVICE = db.viewAll("mpls_core_device_tb", " type = 'BE' ")
    for DEVICE in CORE_DEVICE[1]:
        DEVICE_SESSION = serverCom.NET(JUMP_SERVER_48, DOMAIN_USER_CONFIG, DEVICE)
        JUMP_SESSION = DEVICE_SESSION.JUMP()
        if(JUMP_SESSION[0][0]):
            if(DEVICE[11]=="TELNET"):
                TELNET_SESSION = DEVICE_SESSION.TELNET()
                if(TELNET_SESSION[0][0]):
                    ENABLE_SESSION = DEVICE_SESSION.ENABLE_MODE()
                    if(ENABLE_SESSION[0][0]):
                        COMMAND = { 'CISCO':'show running-config', 'HUAWEI':'display current-configuration', 'OTHER':'show running-config' }
                        WRITE_LONG_COMMAND = DEVICE_SESSION.WRITE_LONG(COMMAND)
                        print(WRITE_LONG_COMMAND)
                    else: return [0, 'ENABLE']
                else: return [0, 'TELNET']
            else: return [0, 'DEVICE']
        else: return [0, 'JUMP']




#########################################################################
def DEACTIVE_DEVICE(DEVICE_IP, COMMENT):
    try:
        db = mysqlDb()
        RESULT = db.edit("mpls_core_device_tb", " coment='"+str(COMMENT)+"', active=0 ", " ip='"+str(DEVICE_IP)+"' ")
        if(RESULT[0]): return 1
        else: return 0
    except Exception as error: return 0

#########################################################################
def COMMENT_DEVICE(COMMENT):
    try:
        db = mysqlDb()
        RESULT = db.edit("mpls_core_device_tb", " coment='"+str(COMMENT)+"', status=1 ", " ip='"+str(DEVICE_IP)+"' ")
        if(RESULT[0]): return 1
        else: return 0
    except Exception as error: return 0

#########################################################################
def CREATE_BACKUP():
    try:
        BACKUP = write.config(str(self.DEVICE_TYPE), str(self.DEVICE_IP)+"__"+str(self.ENABLE[1]), str(self.CONFIG_TEXT))
        if(BACKUP[0]): return 1
        else: return 0
    except Exception as error: return 0

    #########################################################################
    def _SAVE_BACKUP(self):
        try:
            REPORTING_DATE = datetime.datetime.now().date()
            REPORTING_TIME = datetime.datetime.now().time()
            db = mongodb(str(self.DOMAIN_USER_CONFIG_SYNC_MONGO_DB), "core_"+str(self.DEVICE_UNIQUE_VENDOR)+"_config_log")
            SQL_DATA_STRING = {
                        'DID': str(self.DEVICE_ID), 'IP': str(self.DEVICE_IP), 'TYPE': str(self.DEVICE_TYPE),
                        'BRAND': str(self.DEVICE_BRAND), 'CATEGORY': str(self.DEVICE_CATEGORY), 'POSITION': str(self.DEVICE_POSITION),
                        'NAME': str(self.DEVICE_NAME), 'HOST_NAME': str(self.DEVICE_HOST_NAME), 'CONFIG': str(self.CONFIG_TEXT),
                        'REPORTING_DATE': str(REPORTING_DATE), 'REPORTING_TIME': str(REPORTING_TIME)
                        }
            BACKUP = db.add(SQL_DATA_STRING)
            if(BACKUP[0]):
                self.LAG.writeLOG('SAVE_BACKUP_D')
                return 1
            else:
                os.system('COLOR E')
                self.LAG.writeLOG('SAVE_BACKUP_F')
                return 0
        except:
            os.system('COLOR 4')
            self.LAG.writeLOG('SAVE_BACKUP_E')
            return 0
